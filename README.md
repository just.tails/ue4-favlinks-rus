# Полезные материалы по Unreal Engine 4

- [Полезные материалы по Unreal Engine 4](#полезные-материалы-по-unreal-engine-4)
  - [Преамбула](#преамбула)
  - [Основы](#основы)
  - [Игровой дизайн](#игровой-дизайн)
    - [Шутер](#шутер)
  - [Дизайн уровней](#дизайн-уровней)
    - [Интерактивность](#интерактивность)
    - [Открытый мир](#открытый-мир)
  - [Графика](#графика)
    - [Основное](#основное)
    - [Исправления](#исправления)
  - [Программирование](#программирование)
    - [Blueprint Visual Scripting](#blueprint-visual-scripting)
    - [Основы С++](#основы-с)
    - [Сохранение и загрузка](#сохранение-и-загрузка)
    - [Файловая система](#файловая-система)
    - [Организация разработки](#организация-разработки)
    - [Компиляция движка](#компиляция-движка)
  - [Плагины и ассеты](#плагины-и-ассеты)
    - [Популярное](#популярное)
    - [Разработка](#разработка)
  - [Решение проблем](#решение-проблем)

## Преамбула

Данный сборник по движку Unreal Engine обновляется по мере нахождения соответствующих материалов в свободное время и на энтузиазме.

Если вы нашли подходящий материал, то можете самостоятельно добавить нужные изменения через [Pull Request](https://codeberg.org/just.tails/ue4-favlinks-rus/pulls), либо воспользоваться [Issue Tracker](https://codeberg.org/just.tails/ue4-favlinks-rus/issues) и создать там обсуждение по добавлению. В случае, если ресурс в данном сборнике оказался недоступен и/или вы можете заменить ссылку, воспользуйтесь указанной выше процедурой. Каждое предложение будет рассмотрено.

## Основы

* [Введение в разработку компьютерных игр](https://books.ifmo.ru/file/pdf/2646.pdf) - учебное пособие от Университета ИТМО по Unreal Engine 4
* [Полезные ссылки на материалы от uedev](https://uedev.blogspot.com/2016/03/blog-post.html)
* [Unreal Engine 4 Complete Beginners Guide](https://www.youtube.com/playlist?list=PLncmXJdh4q89Fgbg82W0eM9NqsPiHzsBu)
* [Список команд консоли](https://consolehelp.imzlp.com/ue4)

## Игровой дизайн

### Шутер

* [Unreal Immersive FPS. Part 1: Intent, Setup, & a True First Person Character](https://karllewisdesign.com/unreal-immersive-fps-part1/)
* [Unreal Immersive FPS. Part 2: Hand IK and Deadzone Rotation](https://karllewisdesign.com/unreal-immersive-fps-part2/)
* [Unreal Immersive FPS. Part 3: Aiming Down Sights with Procedural Animation…and Math!](https://karllewisdesign.com/unreal-immersive-fps-part3/)
* [Germán López - Weapon FOV & Clipping FIX](https://www.youtube.com/watch?v=B9kg524mwxU)

## Дизайн уровней

### Интерактивность
* [How to create a Destructible Mesh in UE4](https://www.onlinedesignteacher.com/2015/03/how-to-create-destructible-mesh-in-ue4_5.html)
* [Видеоурок по использованию Destructible Mesh](https://www.youtube.com/watch?v=4MaIhIBZq_A)
* [Создаем разрушаемые объекты в Unreal Engine 4 и Blender](https://habr.com/ru/company/pixonic/blog/517684/)

### Открытый мир

* [Композиция мира](https://uengine.ru/site-content/docs/landscape/world-composition)
* [Стриминг уровней](https://ue4daily.com/blog/level-streaming-ue4)

## Графика

### Основное

* [Blender To Unreal Plugin](https://youtu.be/PNGAoKhrBls)
* [Lighting in Unreal Engine 5 for Beginners](https://youtu.be/fSbBsXbjxPo)

### Исправления

* [Fix Weapon Clipping and Object-specific FOV](https://youtu.be/zqfzvHCcvZs)

## Программирование

### Blueprint Visual Scripting

* [Официальное руководство по Blueprint](https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/Blueprints/) (на английском)
* [Русскоязычное руководство UEngine.ru](https://uengine.ru/site-content/docs/blueprints-docs/blueprint) (неполное, устарело)
* [14-й час. Введение в систему визуального программирования Blueprint](https://ue4daily.com/uploads/blog/UE_4_24_light_14ch.pdf) - отрывок из книги с вопросами, примечаниями и заданиями
* [Серия пошаговых туториалов на Habr](https://habr.com/ru/post/344394/) (любительский перевод)
* [Плейлист канала Cyberstars по основам Blueprint](https://www.youtube.com/playlist?list=PL2suyruNHd0iZA-hThptrbmeDN2qbIlSe)
* [Советы по работе с Blueprint Editor в UE4](https://dtf.ru/unrealengine/108110-sovety-po-rabote-s-blueprint-editor-v-unreal-engine-4)
* [Руководство по стилю в Blueprint](https://github.com/Flakky/ue-blueprints-styleguide/tree/russian)

### Основы С++

* [Официальное руководство по C++ в Unreal Engine 4](https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/ProgrammingWithCPP/IntroductionToCPP/) (на английском)
* [Уильям Шериф. Изучаем С++ создавая игры в UE4](https://coddyschool.com/upload/Izuchaem_C__sozdavaya_igry_v_UE4.pdf) - переведенный учебник с постепенным изучением языка С++ от основ до применения в Unreal Engine (устарел)
* [Плейлист канала "Институт марксизма-ленинизма" по программированию в UE4](https://www.youtube.com/playlist?list=PLYpWZxEb36SjZCNTrwc-ZFCLnWg5DmvBR)
* [Основы программирования в UE4](https://www.kodeco.com/185-unreal-engine-4-c-tutorial) (на английском)
* [Шпаргалка по базовым принципам С++ в Unreal Engine](https://uecasts.com/files/unreal-engine-c-plus-plus-cheat-sheet.pdf)
* [Подсказки и трюки в Unreal Engine C++](https://ru.wiki.imlint.org/Unreal_Engine_4_c%2B%2B_tips_and_tricks)

### Сохранение и загрузка

* [Сохранение и загрузка игры](https://awesometuts.com/blog/save-load-game-data-unreal-engine/)
* [Habr. Сохранение и загрузка игры в Unreal Engine](https://habr.com/ru/post/319228/)
* [TomLooman. Unreal Engine C++ Save System (SaveGame)](https://www.tomlooman.com/unreal-engine-cpp-save-system/)
* [BlueBubbleBee. Let’s create a save system in Unreal Engine 4](https://bluebubblebee.medium.com/lets-create-a-save-system-in-unreal-engine-4-276a24e66b2c)
* [GameInstance. How to save data between levels in UE4?](https://stackoverflow.com/questions/65376613/how-to-save-data-between-levels-in-ue4)
* [SaveGame Pointers and Structs](https://www.unrealcommunity.wiki/revisions/6100e8179c9d1a89e0c347d3)

### Файловая система
* [File and Folder Management, Create, Find, Delete](https://www.unrealcommunity.wiki/file-and-folder-management-create-find-delete-et2g64gx)
* [Habr. Асинхронная (и не очень) загрузка данных в Unreal Engine 4](https://habr.com/ru/company/vk/blog/309228/)
* [UE4 – Get Assets by Path in Blueprints with the AssetRegistry](https://isaratech.com/ue4-get-assets-by-path-in-blueprints-with-the-assetregistry/)
* [[UE4] Чтение и запись файла](https://russianblogs.com/article/7034867493/)

### Организация разработки
* [Multi-User Editing - совместная работа над проектами Unreal Engine](https://docs.unrealengine.com/4.27/en-US/ProductionPipelines/MultiUserEditing/)
* [Коллаборация в Unreal Engine](https://docs.unrealengine.com/4.27/en-US/ProductionPipelines/SourceControl/)
* [Использование системы контроля версий Git в UE4](https://www.youtube.com/watch?v=owKSggV_3qc)
* [Краткое руководство по работе с репозиторием](https://uengine.ru/site-content/articles/work-with-git)
* [Unreal Engine Git плагин для работы с большими файлами проектов редактора](https://github.com/ProjectBorealis/UEGitPlugin)

### Компиляция движка

* [Инструкция по компиляции Unreal Engine](https://www.unrealcode.net/content/BuildFromSource.html#opening-the-solution)
* [Решение проблемы компиляции движка на Visual Studio](https://forums.unrealengine.com/t/cant-build-unreal-editor-4-27-2-from-source/654850/2)
* [Решение проблемы запуска движка после успешной компиляции](https://stackoverflow.com/questions/63527007/cannot-compile-unreal-engine-4-25-with-visual-studio-2019-7-2?rq=2)

## Плагины и ассеты

### Популярное
* [Visual Studio Integration Tool](https://www.unrealengine.com/marketplace/en-US/product/visual-studio-integration-tool) - компонент для глубокой интеграции Unreal Engine в Visual Studio 2022
* [ACCURIG 1.1](https://www.youtube.com/watch?v=uNWqMCBtf3U) - бесплатный инструмент для автоматической привязки персонажей к скелету
* [Weapon FOV & Clipping Fix](https://github.com/gerlogu/WeaponFOVAndClippingFix) - исправление шутерной проблемы в виде ассета

### Разработка
* [Скриптинг и автоматизация редактора Unreal](https://docs.unrealengine.com/4.27/en-US/ProductionPipelines/ScriptingAndAutomation/)
* [Создание своего типа ассета в Unreal Engine 4 и кастомизация панели свойств](https://habr.com/ru/post/274159/)
* [Система плагинов и модулей в Unreal Engine 4](https://habr.com/ru/post/275239/)
* [Разрабатываем конфигурируемый плагин для Unreal Engine 4 с нуля](https://proglib.io/p/razrabatyvaem-konfiguriruemyy-plagin-dlya-unreal-engine-4-s-nulya-2021-09-22)
* [Что за модули в Unreal Engine и почему я должен о них париться?](https://proglib.io/p/chto-za-moduli-v-unreal-engine-i-pochemu-ya-dolzhen-o-nih-paritsya-2021-09-22)
* [UE4 C++ Plugin Development Tutorial](https://www.youtube.com/playlist?list=PL7Se41ZzAKZmvCeW9iBpEuh_hxD7bMqMF)
* [How to Make Tools in UE4](https://lxjk.github.io/2019/10/01/How-to-Make-Tools-in-U-E.html)

## Решение проблем
* [Проблема с мгновенным исчезновением (непоявлением) контекстных меню в редакторе на видеокартах Nvidia](https://nvidia.custhelp.com/app/answers/detail/a_id/5157)